use std::process;

use nannou::prelude::*;
use tahga::stroke::watercolor::{StrokeType, WatercolorStroke};

const WIDTH: u32 = 600;
const HEIGHT: u32 = 600;
const CAPTURE: bool = true;

fn main() {
    nannou::app(model)
        .update(update)
        .run();
}

struct Model {
    stroke: WatercolorStroke,

    paused: bool,
    ctrl_key_pressed: bool,
}

fn model(app: &App) -> Model {
    app.set_loop_mode(LoopMode::loop_once());
    app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .key_released(key_released)
        .build()
        .unwrap();

    Model {
        stroke: WatercolorStroke::new()
            .colors(vec!(
                hsla(0.1, 1., 0.2, 0.001),
                hsla(0.2, 1., 0.2, 0.001),
                hsla(0.3, 1., 0.2, 0.001),
                hsla(0.4, 1., 0.2, 0.001),
            ))
            .load(150.)
            .count(5)
            .stroke_type(StrokeType::Left),

        paused: false,
        ctrl_key_pressed: false,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.paused { return; }
}

fn view(app: &App, model: &Model, frame: Frame) {
    if model.paused { return; }
    let draw = app.draw();
    // if app.elapsed_frames() == 1 {
    draw.background().color(WHITE);
    // }

    let rect = app.window_rect();
    for y in -3..4 {
        let level = y as f32 * -100.;
        model.stroke.draw(&draw, vec2(rect.left(), level), vec2(rect.right(), level));
        draw.line()
            .start(vec2(rect.left(), level))
            .end(vec2(rect.right(), level))
        ;
    }

    draw.to_frame(app, &frame).unwrap();

    if CAPTURE {
        let file_path = captured_frame_path(app, &frame);
        app.main_window().capture_frame(file_path);
    }
}

/// React to key-presses
fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::C => {
            if model.ctrl_key_pressed {
                process::exit(0);
            }
        }
        Key::S => {
            let file_path = saved_image_path(app);
            app.main_window().capture_frame(file_path);
        }
        Key::Space => {
            model.paused = !model.paused;
        }
        Key::LControl => {
            model.ctrl_key_pressed = true;
        }
        _other_key => {}
    }
}

/// React to key releases
fn key_released(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::LControl => {
            model.ctrl_key_pressed = false;
        }
        _other_key => {}
    }
}

/// Get the path to the next captured frame
fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("frames")
        .join(format!("frame{:05}", frame.nth()))
        .with_extension("png")
}

/// Get the path to the next saved image
fn saved_image_path(app: &App) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("saved")
        .join(format!("image{:05}", chrono::offset::Local::now()))
        .with_extension("png")
}
